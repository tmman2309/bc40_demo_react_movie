export const localUserServ = {
  get: () => {
    let dataJson = localStorage.getItem("USER_INFOR");
    return JSON.parse(dataJson);
    // return null hoặc object
    // null, undefined, "", 0, false, NaN: Falsy (sai)
  },
  set: (userInfo) => {
    let dataJson = JSON.stringify(userInfo);
    localStorage.setItem("USER_INFOR", dataJson);
  },
  remove: () => {
    localStorage.removeItem("USER_INFOR")
  },
};
