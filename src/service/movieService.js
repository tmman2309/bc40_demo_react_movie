import { https } from "./config";

export const movieServ = {
  getMovieList: () => {
    // Cách 1:
    // return axios({
    //   url: `${BASE_URL}/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP00`,
    //   method: "GET",
    //   headers: configHeaders(),
    // })

    // Cách 2:
    return https.get("/api/QuanLyPhim/LayDanhSachPhim?maNhom=GP00");
  },
  getMovieByTheater: () => {
    return https.get("/api/QuanLyRap/LayThongTinLichChieuHeThongRap");
  },

  getDetailMovie: (maPhim) => {
    return https.get(`/api/QuanLyPhim/LayThongTinPhim?MaPhim=${maPhim}`);
  },
};
