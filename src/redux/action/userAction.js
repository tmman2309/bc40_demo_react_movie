import { message } from "antd";
import { localUserServ } from "../../service/localService";
import { userServ } from "../../service/userService";
import { USER_LOGIN } from "../constant/userConstant";

export const setLoginAction = (value) => ({
  // value đến từ response axios
  type: USER_LOGIN,
  payload: value,
});

// redux thunk gọi api trong action
export const setLoginActionService = (value, onSuccess) => {
  // value đến từ thẻ form của antd
  return (dispatch) => {
    userServ
      .postLogin(value)
      .then((res) => {
        console.log(res);
        // Cách 1:
        // dispatch({
        //   type: USER_LOGIN,
        //   payload: res.data.content,
        // });

        // Cách 2:
        localUserServ.set(res.data.content);
        onSuccess();
        dispatch(setLoginAction(res.data.content));
      })
      .catch((err) => {
        console.log(err);
      });
  };
};
