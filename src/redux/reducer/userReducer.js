import { localUserServ } from "../../service/localService";
import { USER_LOGIN } from "../constant/userConstant";

const initialState = {
  userInfor: localUserServ.get(),
};

let userReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case USER_LOGIN: {
      return {...state, userInfor: payload}
    }

    default:
      return state;
  }
};

export default userReducer;
