import moment from "moment/moment";
import React from "react";

export default function ItemTabMovie({ phim }) {
  return (
    <div className="p-5 flex space-x-6">
      <img src={phim.hinhAnh} className="w-36 h-36" />
      <div>
        <h3 className="font-medium text-xl mb-2">{phim.tenPhim}</h3>
        <div className="grid grid-cols-3 gap-5">
          {phim.lstLichChieuTheoPhim.slice(0,9).map((item,index) => {
            return (
              <span key={index} className="rounded p-2 bg-red-500 text-white font-medium">
                {/* moment js dùng để format thời gian */}
                {moment(item.ngayChieuGioChieu).format("DD/MM/YYYY ~ hh:mm")}
              </span>
            );
          })}
        </div>
      </div>
    </div>
  );
}
