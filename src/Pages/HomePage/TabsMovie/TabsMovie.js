import React, { useEffect, useState } from "react";
import { movieServ } from "../../../service/movieService";
import { Tabs } from "antd";
import ItemTabMovie from "./ItemTabMovie";

const onChange = (key) => {
  console.log(key);
};

export default function TabsMovie() {
  const [heThongRap, setHeThongRap] = useState([]);

  let renderHeThongRap = () => {
    return heThongRap.map((rap) => {
      return {
        key: rap.maHeThongRap,
        label: <img className="h-16" src={rap.logo} />,
        children: (
          <Tabs
            style={{ height: "500px" }}
            tabPosition="left"
            defaultActiveKey="1"
            items={rap.lstCumRap.map((cumRap) => {
              return {
                key: cumRap.tenCumRap,
                label: <div>{cumRap.tenCumRap}</div>,
                children: (
                  <div
                    style={{ height: "500px" }}
                    className="overflow-y-scroll"
                  >
                    {cumRap.danhSachPhim.map((item,index) => {
                      return <ItemTabMovie key={index} phim={item} />;
                    })}
                  </div>
                ),
              };
            })}
            onChange={onChange}
          />
        ),
      };
    });
  };

  useEffect(() => {
    movieServ
      .getMovieByTheater()
      .then((res) => {
        setHeThongRap(res.data.content);
        console.log(res);
      })
      .catch((err) => {
        console.log(err);
      });
  }, []);

  return (
    <div className="container">
      <Tabs
        style={{ height: "500px" }}
        tabPosition="left"
        defaultActiveKey="1"
        items={renderHeThongRap()}
        onChange={onChange}
      />
    </div>
  );
}
