import React from "react";
import { Button, Checkbox, Form, Input, message } from "antd";
import { userServ } from "../../service/userService";
import { localUserServ } from "../../service/localService";
import { useNavigate } from "react-router-dom";
import { useDispatch } from "react-redux";
import Lottie from "lottie-react";
import login_animate from "../../asset/login_animate.json";
import { setLoginAction, setLoginActionService } from "../../redux/action/userAction";
import { USER_LOGIN } from "../../redux/constant/userConstant";

export default function LoginPage() {
  let navigate = useNavigate();
  let dispatch = useDispatch();

  // Cách 1: redux thông thường (call API -> dispatch -> action -> store của redux)
  const onFinish = (values) => {
    console.log("Success:", values);
    userServ
      .postLogin(values)
      .then((res) => {
        message.success("Login thành công!");
        // lưu thông tin user vào localStorage
        localUserServ.set(res.data.content);
        // Cách 1:
        // dispatch({
        //   type: USER_LOGIN,
        //   payload: res.data.content,
        // });

        // Cách 2:
        dispatch(setLoginAction(res.data.content));
        // chuyển hướng user tới HomePage
        navigate("/");
        console.log(res);
      })
      .catch((err) => {
        message.error("Login thất bại!");
        console.log(err);
      });
  };
  // Cách 2: redux thunk (dispatch -> action (gọi api trong action luôn))
  const onFinishThunk = (values) => {
    let onSuccess = () => {
      message.success("Login thành công!");
      navigate("/");
    }

    dispatch(setLoginActionService(values, onSuccess))
  }
  const onFinishFailed = (errorInfo) => {
    console.log("Failed:", errorInfo);
  };

  return (
    <div>
      <div className="h-screen w-screen flex bg-orange-500 justify-center items-center">
        <div className="container mx-auto p-5 bg-white rounded flex justify-center items-center">
          <div className="w-1/2 h-full">
            <Lottie animationData={login_animate} loop={true} />
          </div>
          <div className="w-1/2 h-full">
            <Form
              name="basic"
              labelCol={{
                span: 8,
              }}
              wrapperCol={{
                span: 24,
              }}
              style={{
                maxWidth: "100%",
              }}
              initialValues={{
                remember: true,
              }}
              onFinish={onFinishThunk}
              onFinishFailed={onFinishFailed}
              autoComplete="off"
              layout="vertical"
            >
              <Form.Item
                label="Username"
                name="taiKhoan"
                rules={[
                  {
                    required: true,
                    message: "Please input your username!",
                  },
                ]}
              >
                <Input />
              </Form.Item>

              <Form.Item
                label="Password"
                name="matKhau"
                rules={[
                  {
                    required: true,
                    message: "Please input your password!",
                  },
                ]}
              >
                <Input.Password />
              </Form.Item>

              <Form.Item
                wrapperCol={{
                  span: 24,
                }}
                className="flex justify-center items-center"
              >
                <Button
                  className="bg-orange-500 hover:text-white hover:border-hidden"
                  htmlType="submit"
                >
                  Submit
                </Button>
              </Form.Item>
            </Form>
          </div>
        </div>
      </div>
    </div>
  );
}
