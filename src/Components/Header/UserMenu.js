import React from "react";
import { useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { localUserServ } from "../../service/localService";
import UserDropdown from "./UserDropdown";

export default function UserMenu() {
  let userInfor = useSelector((state) => {
    return state.userReducer.userInfor;
  });

  let handleLogout = () => {
    localUserServ.remove();
    // 1: load lại trang
    window.location.reload();
    // 2: redirect
    // window.location.href="/login"
  };

  let renderContent = () => {
    let buttonCss = "px-5 py-2 border-2 border-black rounded";
    if (userInfor) {
      return (
        <div className="flex items-center space-x-5">
          <UserDropdown
            logoutBtn={
              <button onClick={handleLogout} className={buttonCss}>
                Đăng xuất
              </button>
            }
            user={userInfor}
          />
        </div>
      );
    } else {
      return (
        <>
          <NavLink to="/login">
            <button className={buttonCss}>Đăng nhập</button>
          </NavLink>
          <button className={buttonCss}>Đăng ký</button>
        </>
      );
    }
  };

  return <div className="space-x-5">{renderContent()}</div>;
}
