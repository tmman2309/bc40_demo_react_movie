import { DownOutlined } from "@ant-design/icons";
import { Dropdown, Space } from "antd";

const UserDropdown = ({ user, logoutBtn }) => (
  <Dropdown
    menu={{
      items: [
        {
          label: <a href="/">Thông tin</a>,
          key: "0",
        },
        {
          label: logoutBtn,
          key: "1",
        },
      ],
    }}
    trigger={["click"]}
  >
    <a onClick={(e) => e.preventDefault()}>
      <Space>
        {user.hoTen}
        <DownOutlined />
      </Space>
    </a>
  </Dropdown>
);
export default UserDropdown;
